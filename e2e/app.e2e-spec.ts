import { RestmqClientPage } from './app.po';

describe('restmq-client App', () => {
  let page: RestmqClientPage;

  beforeEach(() => {
    page = new RestmqClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
