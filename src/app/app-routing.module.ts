import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {QueueComponent} from './queues/queue.component';

const routes: Routes = [
    {path: 'queues/:name', component: QueueComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
