import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import { AppRoutingModule } from './app-routing.module';

import {AppComponent} from './app.component';
import {QueuesComponent} from './queues/queues.component';
import {QueueComponent} from './queues/queue.component';
import {QueueService} from './queues/queue.service';
import {MessageService} from './messages/message.service';

@NgModule({
    declarations: [
        AppComponent,
        QueuesComponent,
        QueueComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        AppRoutingModule
    ],
    providers: [
        QueueService,
        MessageService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
