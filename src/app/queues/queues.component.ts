import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {Queue} from './queue';
import {QueueService} from './queue.service';

@Component({
    selector: 'queue-list',
    templateUrl: './queues.component.html',
    styleUrls: ['./queues.component.scss']
})

export class QueuesComponent implements OnInit {
    queues: Queue[];
    selected: Queue;

    constructor(
        private queueServrice: QueueService,
        private router: Router) {}

    ngOnInit(): void {
        this.getQueues();
    }

    getQueues(): void {
        this.queueServrice
            .getQueues()
            .then(
            queues => this.queues = queues
            );
    }

    onSelect(queue: Queue): void {
        this.selected = queue;
        this.router.navigate(['/queues', this.selected.name]);
    }

    create(name: string): void {
        name = name.trim();
        if (!name) {
            return;
        }
        this.queueServrice.create(name)
            .then(queue => {
                this.queues.push(queue);
                this.selected = null;
            });
    }
}