import {Injectable} from '@angular/core';
import {Headers, Http, Response} from '@angular/http';

import 'rxjs/add/operator/toPromise';

import {Queue} from './queue';

@Injectable()
export class QueueService {
    private headers = new Headers({'Content-Type': 'application/json'});
    private queuesUrl = 'http://127.0.0.1:8080/api/queues';

    constructor(private http: Http) {}

    getQueues(): Promise<Queue[]> {
        return this.http.get(this.queuesUrl)
            .toPromise()
            .then(response => response.json() as Queue[])
            .catch(this.handleError);
    }

    getQueue(name: string): Promise<Queue> {
        const url = `${this.queuesUrl}/${name}`;
        return this.http.get(url)
            .toPromise()
            .then(response => response.json() as Queue)
            .catch(this.handleError);
    }

    create(name: string): Promise<Queue> {
        return this.http
            .post(this.queuesUrl, JSON.stringify({name: name}), {headers: this.headers})
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}