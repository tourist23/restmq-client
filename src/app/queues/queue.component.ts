import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {Location} from '@angular/common';
import 'rxjs/add/operator/switchMap';

import {Message} from '../messages/message';
import {Queue} from './queue';
import {QueueService} from './queue.service';
import {MessageService} from '../messages/message.service';

@Component({
    selector: 'queue',
    templateUrl: './queue.component.html',
    styleUrls: ['./queue.component.scss', '../messages/message.component.scss']
})

export class QueueComponent implements OnInit {
    messages: Message[] = [];
    queue: Queue;

    constructor(
        private queueService: QueueService,
        private messageService: MessageService,
        private route: ActivatedRoute,
        private location: Location
    ) {}

    ngOnInit(): void {
        this.route.params
            .switchMap((params: Params) => this.queueService.getQueue(params['name']))
            .subscribe(queue => {this.queue = queue, this.messages = []});
    }

    next(): void {
        this.messageService.getNextMessage(this.queue.name)
            .then(message => {
                if (message) {
                    this.messages.unshift(message)
                    this.messageService.delete(
                        this.queue.name, message.messageId);
                }
            });
    }

    create(content: string): void {
        content = content.trim();
        if (!content) {
            return;
        }
        this.messageService.create(this.queue.name, content);
    }
}