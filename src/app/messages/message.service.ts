import {Injectable} from '@angular/core';
import {Headers, Http, Response} from '@angular/http';

import 'rxjs/add/operator/toPromise';

import {Message} from './message';

@Injectable()
export class MessageService {
    private headers = new Headers({'Content-Type': 'application/json'});
    private queuesUrl = 'http://127.0.0.1:8080/api/queues';
    
    constructor(private http: Http) {}
    
    getNextMessage(queueName: string): Promise<Message> {
        const url = `${this.queuesUrl}/${queueName}/messages`;
        return this.http.get(url)
            .toPromise()
            .then(response => response.json() as Message)
            .catch(this.handleError);
    }

    create(queueName: string, content: string): Promise<Message> {
        const url = `${this.queuesUrl}/${queueName}/messages`;
        return this.http
            .post(url, JSON.stringify(content), {headers: this.headers})
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    delete(queueName: string, id: string): Promise<void> {
        const url = `${this.queuesUrl}/${queueName}/messages/${id}`;
        return this.http
            .delete(url)
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }
    
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}