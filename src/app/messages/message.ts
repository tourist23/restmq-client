export class Message {
  messageId: string;
  content: string;
  created: number;
}
